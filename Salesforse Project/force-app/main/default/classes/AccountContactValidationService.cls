public with sharing class AccountContactValidationService {
    
    public void validateInsert(List<AccountContact__c> accountContacts) {

        if(!CheckRecursive.firstcall) {
            CheckRecursive.firstcall = true;
            for (AccountContact__c accountContact : accountContacts){
                accountContact.isPrimary__c = true;
            }

            List<AccountContact__c> contacts = [SELECT Contact__c, isPrimary__c FROM AccountContact__c WHERE isPrimary__c=true];
            for (AccountContact__c accountContact : accountContacts){
                for (AccountContact__c contact : contacts){
                    if (accountContact.Contact__c == contact.Contact__c){
                        contact.isPrimary__c = false;
                        update contact;
                    }
                }
            }
        }
    }

    public void validateUpdate(List<AccountContact__c> accountContacts) {

        if(!CheckRecursive.firstcall) {
            CheckRecursive.firstcall = true;

            List<AccountContact__c> contactsIsPrimaryTrue = [SELECT Contact__c, isPrimary__c FROM AccountContact__c contacts WHERE isPrimary__c=true];
            List<AccountContact__c> contactsIsPrimaryFalse = [SELECT Contact__c, isPrimary__c, LastModifiedDate FROM AccountContact__c WHERE isPrimary__c=false];
            List<AggregateResult> contactsLastUpdated = [SELECT MAX(LastModifiedDate) max, Contact__c FROM AccountContact__c GROUP BY Contact__c];
            for (AccountContact__c accountContact : accountContacts){
                if (accountContact.isPrimary__c){
                    for (AccountContact__c contact : contactsIsPrimaryTrue){
                        if (accountContact.Contact__c == contact.Contact__c){
                            
                            contact.isPrimary__c = false;
                            update contact;
                        }
                    }
                } else {
                    for (AggregateResult lastUpdateContact : contactsLastUpdated){
                        if (accountContact.Contact__c == lastUpdateContact.get('Contact__c')){
                            for (AccountContact__c contact : contactsIsPrimaryFalse){
                                if (lastUpdateContact.get('max') == contact.LastModifiedDate){
                                    
                                    contact.isPrimary__c = true;
                                    update contact;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
