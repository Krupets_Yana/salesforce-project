public with sharing class AccountContactTriggerHandler {
    
    public static void beforeInsert(List<AccountContact__c> accountContacts) {
        new AccountContactValidationService().validateInsert(accountContacts);
    }

    public static void afterInsert(List<AccountContact__c> accountContacts) {
    }

    public static void beforeUpdate(List<AccountContact__c> accountContacts, List<AccountContact__c> oldList) {
        new AccountContactValidationService().validateUpdate(accountContacts);
    }

    public static void afterUpdate(List<AccountContact__c> accountContacts, List<AccountContact__c> oldList) {
    }
}