trigger CheckWorkplace on AccountContact__c (before insert, after insert, before update, after update) {

    if (Trigger.isInsert){
        if (Trigger.isBefore){
            AccountContactTriggerHandler.beforeInsert(Trigger.new);
        } else if (Trigger.isAfter){
            AccountContactTriggerHandler.afterInsert(Trigger.new);
        }
    }
    else if (Trigger.isUpdate){
        if (Trigger.isBefore){
            AccountContactTriggerHandler.beforeUpdate(Trigger.new, Trigger.old);
        } else if (Trigger.isAfter){
            AccountContactTriggerHandler.afterUpdate(Trigger.new, Trigger.old);
        }
    }
}